﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using GeoAPI.CoordinateSystems.Transformations;
using NetTopologySuite.CoordinateSystems.Transformations;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO.GML2;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Run().Wait();

            Console.ReadKey();
        }

        static async Task Run()
        {                        
            var token = await GetToken();
            var wfs = await GetWfs(token);

            //http://stackoverflow.com/questions/1818147/help-parsing-gml-data-using-c-sharp-linq-to-xml

            var doc = XDocument.Parse(wfs);
            XNamespace gml = "http://www.opengis.net/gml";

            var query = doc.Descendants(gml + "MultiSurface");
            var reader = new GMLReader();                

            foreach (var xElement in query)
            {                
                var geometry = reader.Read(xElement.ToString());
                //var bufferedGeometry = geometry.Buffer(50);          
                Console.WriteLine(geometry.Area);
            }
        }

        private static async Task<string> GetWfs(string token)
        {
            string wfs;

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri =
                        new Uri(
                            "https://uat-data.landmark.co.uk/v1/data01/ECSiteSensitivity?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typename=Polys00861&srsName=EPSG%3A27700&BBOX=334000%2C139000%2C334100%2C139100%2CEPSG%3A27700"),
                    Method = HttpMethod.Get,
                };
                request.Headers.Add("Authorization", "Bearer " + token);

                var response = await client.SendAsync(request);

                wfs = await response.Content.ReadAsStringAsync();
            }

            return wfs;
        }

        private static async Task<string> GetToken()
        {
            string token;

            using (var client = new HttpClient())
            {
                var formData = new Dictionary<string, string>
                {
                    {"resource", "https://lmkfeaturesnl.onmicrosoft.com/FeaturesUAT"},
                    {"client_id", "33fc0d48-aa04-454e-b50b-877990767246"},
                    {"client_secret", "WYVSaEEUhWIdbGFQ5IiTvtwh4kZuC83rFgbiqXARWuY="},
                    {"grant_type", "client_credentials"}
                };
                var content = new FormUrlEncodedContent(formData);

                var response =
                    await
                        client.PostAsync("https://login.windows.net/lmkfeaturesnl.onmicrosoft.com/oauth2/token", content);

                var body = await response.Content.ReadAsStringAsync();

                dynamic tokenResponse = JObject.Parse(body);

                token = tokenResponse.access_token;
            }

            return token;
        }
    }
}
